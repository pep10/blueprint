FORMS += \
    $$PWD/memorydumppane.ui

HEADERS += \
    $$PWD/amemorychip.h \
    $$PWD/amemorydevice.h \
    $$PWD/mainmemory.h \
    $$PWD/memorychips.h \
    $$PWD/memorydumppane.h

SOURCES += \
    $$PWD/amemorychip.cpp \
    $$PWD/amemorydevice.cpp \
    $$PWD/mainmemory.cpp \
    $$PWD/memorychips.cpp \
    $$PWD/memorydumppane.cpp
