FORMS += \
    $$PWD/memorytracepane.ui

HEADERS += \
    $$PWD/memorycellgraphicsitem.h \
    $$PWD/memorytracepane.h \
    $$PWD/stacktrace.h \
    $$PWD/typetags.h

SOURCES += \
    $$PWD/memorycellgraphicsitem.cpp \
    $$PWD/memorytracepane.cpp \
    $$PWD/stacktrace.cpp \
    $$PWD/typetags.cpp
