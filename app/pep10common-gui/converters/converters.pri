FORMS += \
    $$PWD/byteconverterbin.ui \
    $$PWD/byteconverterchar.ui \
    $$PWD/byteconverterdec.ui \
    $$PWD/byteconverterhex.ui \
    $$PWD/byteconverterinstr.ui

HEADERS += \
    $$PWD/byteconverterbin.h \
    $$PWD/byteconverterchar.h \
    $$PWD/byteconverterdec.h \
    $$PWD/byteconverterhex.h \
    $$PWD/byteconverterinstr.h

SOURCES += \
    $$PWD/byteconverterbin.cpp \
    $$PWD/byteconverterchar.cpp \
    $$PWD/byteconverterdec.cpp \
    $$PWD/byteconverterhex.cpp \
    $$PWD/byteconverterinstr.cpp
