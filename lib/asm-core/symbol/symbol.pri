HEADERS += \
    $$PWD/symbolentry.h \
    $$PWD/symboltable.h \
    $$PWD/symboltypes.h \
    $$PWD/symbolvalue.h

SOURCES += \
    $$PWD/symbolentry.cpp \
    $$PWD/symboltable.cpp \
    $$PWD/symboltypes.cpp \
    $$PWD/symbolvalue.cpp
