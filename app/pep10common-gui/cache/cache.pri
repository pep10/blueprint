FORMS += \
    $$PWD/cacheaddresstranslator.ui \
    $$PWD/cacheconfig.ui \
    $$PWD/cacheview.ui

HEADERS += \
    $$PWD/cache.h \
    $$PWD/cacheaddresstranslator.h \
    $$PWD/cachealgs.h \
    $$PWD/cacheconfig.h \
    $$PWD/cacheline.h \
    $$PWD/cachememory.h \
    $$PWD/cachereplace.h \
    $$PWD/cacheview.h

SOURCES += \
    $$PWD/cacheaddresstranslator.cpp \
    $$PWD/cachealgs.cpp \
    $$PWD/cacheconfig.cpp \
    $$PWD/cacheline.cpp \
    $$PWD/cachememory.cpp \
    $$PWD/cachereplace.cpp \
    $$PWD/cacheview.cpp
