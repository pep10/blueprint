FROM ubuntu

RUN apt update && DEBIAN_FRONTEND=noninteractive apt install -y git cmake build-essential

# Configure Pep/10 sources.
COPY /lib /pep10/lib/source
COPY /cmake_scripts /pep10/lib/cmake_scripts
RUN cmake -B /pep10/lib/build /pep10/lib/source

# Build Pep/10 sources.
WORKDIR /pep10/lib/build
RUN make