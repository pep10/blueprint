FORMS += \
    $$PWD/asmcpupane.ui

HEADERS += \
    $$PWD/asmcpupane.h \
    $$PWD/interfaceisacpu.h \
    $$PWD/memoizerhelper.h

SOURCES += \
    $$PWD/asmcpupane.cpp \
    $$PWD/interfaceisacpu.cpp \
    $$PWD/memoizerhelper.cpp
