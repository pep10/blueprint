# Pep10Suite
Source code for Pep/10 project.

ISA/ASM/LG directories should have no hard dependencies on Qt, since the STL is plenty powerful.

application+gui code belongs in -lib, while the actual application files belong in -gui.

-tui is for the Pep/10 term terminal UI.
