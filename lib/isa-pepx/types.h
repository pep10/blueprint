#ifndef TYPES_H
#define TYPES_H
#include <cstdint>

namespace  PepCore{
    using CPURegisters_number_t = uint8_t;
    using CPUStatusBits_name_t = uint8_t;
}

#endif // TYPES_H
