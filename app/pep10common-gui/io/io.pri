FORMS += \
    $$PWD/inputpane.ui \
    $$PWD/iowidget.ui \
    $$PWD/outputpane.ui \
    $$PWD/terminalpane.ui

HEADERS += \
    $$PWD/inputpane.h \
    $$PWD/iowidget.h \
    $$PWD/outputpane.h \
    $$PWD/terminalpane.h

SOURCES += \
    $$PWD/inputpane.cpp \
    $$PWD/iowidget.cpp \
    $$PWD/outputpane.cpp \
    $$PWD/terminalpane.cpp
