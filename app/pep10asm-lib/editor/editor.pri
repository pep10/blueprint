FORMS += \
    $$PWD/asmobjectcodepane.ui \
    $$PWD/asmprogramlistingpane.ui \
    $$PWD/asmsourcecodepane.ui \
    $$PWD/assemblerpane.ui

HEADERS += \
    $$PWD/asmobjectcodepane.h \
    $$PWD/asmprogramlistingpane.h \
    $$PWD/asmsourcecodepane.h \
    $$PWD/assemblerpane.h

SOURCES += \
    $$PWD/asmobjectcodepane.cpp \
    $$PWD/asmprogramlistingpane.cpp \
    $$PWD/asmsourcecodepane.cpp \
    $$PWD/assemblerpane.cpp
