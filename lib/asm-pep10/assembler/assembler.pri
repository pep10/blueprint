HEADERS += \
    $$PWD/asmargument.h \
    $$PWD/asmcode.h \
    $$PWD/asmparserhelper.h \
    $$PWD/asmprogram.h \
    $$PWD/asmprogrammanager.h

SOURCES += \
    $$PWD/asmargument.cpp \
    $$PWD/asmcode.cpp \
    $$PWD/asmparserhelper.cpp \
    $$PWD/asmprogram.cpp \
    $$PWD/asmprogrammanager.cpp
